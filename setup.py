#!/usr/bin/env python3

from distutils.core import setup

from os import path
from codecs import open
import glob
import subprocess

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='veracrypt-mounter',
    version='0.1',
    description='Mount TrueCrypt and VeraCrypt volumes',
    long_description=long_description,
    author='Tails developers',
    author_email='tails@boum.org',
    license='GPLv3',
    scripts=['veracrypt-mounter'],
    packages=['veracrypt_mounter'],
    data_files=[
        ('/usr/share/veracrypt-mounter/ui', glob.glob('ui/*')),
        ('/usr/share/pixmaps', ['data/veracrypt-mounter.png']),
        ('/usr/local/share/applications', ['data/veracrypt-mounter.desktop']),
        ('/usr/local/share/mime/packages', ['data/veracrypt-mounter.xml'])
    ],
    requires=['gi'],
)

subprocess.check_call(["update-mime-database", "/usr/local/share/mime"])
subprocess.check_call(["update-desktop-database", "/usr/local/share/applications"])
